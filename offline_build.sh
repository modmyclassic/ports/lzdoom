#!/bin/sh

CURRENT_LOC=$(pwd)

apt-get update
apt-get install -y cmake libfluidsynth-dev:armhf libgme-dev:armhf libopenal-dev:armhf \
                   libmpg123-dev:armhf libsndfile1-dev:armhf libwildmidi-dev:armhf
cp "zdoom_armhf_inc.zip" "/usr/arm-linux-gnueabihf/psc_SDK/"
cd "/usr/arm-linux-gnueabihf/psc_SDK/"
unzip "zdoom_armhf_inc.zip"
cd "${CURRENT_LOC}"
touch "/usr/bin/arm-linux-gnueabihf-g++"
. classic_set_env_psc_6
mkdir -p "build"
cd "build"
wget -nc http://zdoom.org/files/fmod/fmodapi44464linux.tar.gz && tar -xzf "fmodapi44464linux.tar.gz"
cmake -DARMv7_COMPILE=YES -DFLUIDSYNTH_INCLUDE_DIR="/usr/include/fluidsynth" "../"
sed -i 's/-j -f/-j 4 -f/g' "/usr/bin/classic_make_psc"
classic_make_psc "Makefile"
arm-linux-gnueabihf-strip "lzdoom"
cd "../classic" && make -f "Makefile.pack_psc_eris"
cd "../classic" && make -f "Makefile.pack_psc_eris_brutal"
cd "${CURRENT_LOC}/classic" && echo -e "cd /var/www/html/project_eris/mods/\nput *.mod" | sftp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.keys/release_private_key.pem docker@classicmodscloud.com